//
//  MainMenuScene.h
//  p08-hamel
//
//  Created by David Hamel on 5/10/17.
//  Copyright © 2017 dhamel1. All rights reserved.
//

#ifndef MainMenuScene_h
#define MainMenuScene_h

#import <SpriteKit/SpriteKit.h>

@interface MainMenuScene : SKScene

@end
#endif /* MainMenuScene_h */
