//
//  MainMenuScene.m
//  p08-hamel
//
//  Created by David Hamel on 5/10/17.
//  Copyright © 2017 dhamel1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Foundation/Foundation.h>
#import "MainMenuScene.h"
#import "GameScene.h"
#import "math.h"

@interface MainMenuScene () {
    SKLabelNode* instruct1;
    SKLabelNode* instruct2;
    SKLabelNode* instruct3;
    SKLabelNode* instruct4;
    SKLabelNode* instruct5;
    SKSpriteNode *bottleSprite;
    
}

@end

@implementation MainMenuScene

- (void)didMoveToView:(SKView *)view {
    self.backgroundColor = [SKColor blackColor];
    [self startScene];
    
}

-(void) startScene {
    [self createStartLabels];
    //https://makeapppie.com/2014/04/01/slippyflippy-1-1-adding-a-fading-in-and-out-label-with-background-in-spritekit/
    //found code here for fading in an out, then made it ran forever
    SKAction *flashAction = [SKAction sequence:@[
                                                 [SKAction fadeInWithDuration:1],
                                                 [SKAction waitForDuration:0],
                                                 [SKAction fadeOutWithDuration:1]
                                                 ]];
    SKAction *repeat = [SKAction repeatActionForever:flashAction];
    [instruct5 runAction:repeat];
}

-(void) createStartLabels {
    instruct1 = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    instruct1.fontSize = 60;
    instruct1.fontColor = [SKColor whiteColor];
    instruct1.position = CGPointMake(CGRectGetMidX(self.frame), self.frame.size.height -100);
    instruct1.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    [instruct1 setText:@"Math Attack"];
    [self addChild:instruct1];
    
    instruct2 = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    instruct2.fontSize = 25;
    instruct2.fontColor = [SKColor whiteColor];
    instruct2.position = CGPointMake(CGRectGetMidX(self.frame),self.frame.size.height -200);
    instruct2.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    [instruct2 setText:@"Touch screen to shoot"];
    [self addChild:instruct2];
    
    instruct3 = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    instruct3.fontSize = 25;
    instruct3.fontColor = [SKColor whiteColor];
    instruct3.position = CGPointMake(CGRectGetMidX(self.frame),self.frame.size.height -300);
    instruct3.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    [instruct3 setText:@"Missles go where you touch"];
    [self addChild:instruct3];
    
    instruct4 = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    instruct4.fontSize = 25;
    instruct4.fontColor = [SKColor whiteColor];
    instruct4.position = CGPointMake(CGRectGetMidX(self.frame), self.frame.size.height-400);
    instruct4.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    [instruct4 setText:@"Shoot divisable numbers"];
    [self addChild:instruct4];
    
    instruct5 = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    instruct5.fontSize = 20;
    instruct5.fontColor = [SKColor redColor];
    instruct5.position = CGPointMake(CGRectGetMidX(self.frame), self.frame.size.height - 500);
    instruct5.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    [instruct5 setText:@"Bad shots result in Mission Failure"];
    [self addChild:instruct5];
    [self generateEnemyNumbers];
    
    
}
- (CGPoint)randomPointOutsideRect:(CGRect)rect {
    NSUInteger random = arc4random_uniform(4);
    UIRectEdge edge = 1 << random; // UIRectEdge enum values are defined with bit shifting
    
    CGPoint randomPoint = CGPointZero;
    if (edge == UIRectEdgeTop || edge == UIRectEdgeBottom) {
        randomPoint.x = arc4random_uniform(CGRectGetWidth(rect)) + CGRectGetMinX(rect);
        if (edge == UIRectEdgeTop) {
            randomPoint.y = CGRectGetMinY(rect) - 100;
        }
        else {
            randomPoint.y = CGRectGetMaxY(rect) + 100;
        }
    }
    else if (edge == UIRectEdgeLeft || edge == UIRectEdgeRight) {
        randomPoint.y = arc4random_uniform(CGRectGetHeight(rect)) + CGRectGetMinY(rect);
        if (edge == UIRectEdgeLeft) {
            randomPoint.x = CGRectGetMinX(rect) - 100;
        }
        else {
            randomPoint.x = CGRectGetMaxX(rect) + 100;
        }
    }
    
    return randomPoint;
}

-(void)generateEnemyNumbers{
    //while(!gameOver){
    //SKNode* numberHolder;
    //numberHolder = [SKSpriteNode spriteNodeWithColor:[SKColor whiteColor] size:bulletSize];
    //numberHolder.name = @"Asteroid";
    CGPoint  point = [self randomPointOutsideRect:self.frame];
    SKNode * numberHolder;
    SKTexture *asteroidTexture = [SKTexture textureWithImageNamed:@"asteroid2.png"];
    asteroidTexture.filteringMode = SKTextureFilteringNearest;
    numberHolder = [SKSpriteNode spriteNodeWithTexture:asteroidTexture];
    [numberHolder setScale:.2];
    //numberHolder.physicsBody.dynamic = NO;
    float x = arc4random_uniform((CGRectGetMaxX(self.view.frame))) + CGRectGetMaxX(self.view.frame);
    float y = arc4random_uniform((CGRectGetMaxY(self.view.frame))) + CGRectGetMaxY(self.view.frame);
    numberHolder.position = point;
    numberHolder.zPosition = -1;
    //[numberHolder setName:@"numberLabel"];
    //numberLabel.center = (CGPointMake(x,y));
    CGPoint  destination = CGPointMake(0,0);
    if((point.x < CGRectGetMidX(self.frame)) && (point.y < CGRectGetMidY(self.frame))){
        destination = CGPointMake(CGRectGetMaxX(self.frame), CGRectGetMaxY(self.frame));
    }
    
    if((point.x > CGRectGetMidX(self.frame)) && (point.y < CGRectGetMidY(self.frame))){
        destination = CGPointMake(CGRectGetMinX(self.frame), CGRectGetMaxY(self.frame));
    }
    
    if((point.x < CGRectGetMidX(self.frame)) && (point.y > CGRectGetMidY(self.frame))){
        destination = CGPointMake(CGRectGetMaxX(self.frame), CGRectGetMinY(self.frame));
    }
    
    if((point.x > CGRectGetMidX(self.frame)) && (point.y > CGRectGetMidY(self.frame))){
        destination = CGPointMake(CGRectGetMinX(self.frame), CGRectGetMinY(self.frame));
    }
    
    SKAction* numberAction = [SKAction sequence:@[[SKAction moveTo:destination duration:2.5],
                                                  [SKAction waitForDuration:3.0/60.0],
                                                  [SKAction removeFromParent]]];
    [numberHolder runAction:numberAction];
    [self addChild:numberHolder];
    float randomNum = arc4random_uniform(4) * 0.4 + 0.25;
    [self performSelector:@selector(generateEnemyNumbers) withObject:nil afterDelay:1.5];
    //[numberHolder addChild:numberLabel];
    //}
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;
    
    // Create and configure the scene.
    SKScene * scene = [GameScene sceneWithSize:skView.bounds.size];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:scene];
}

-(void)update:(CFTimeInterval)currentTime {
}

@end
