//
//  GameScene.h
//  p08-hamel
//
//  Created by David Hamel on 5/2/17.
//  Copyright © 2017 dhamel1. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene
@property (strong) NSMutableArray* tapQueue;
@property (strong) NSMutableArray* locationQueue;
@end
