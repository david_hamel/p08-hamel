//
//  GameScene.m
//  p08-hamel
//
//  Created by David Hamel on 5/2/17.
//  Copyright © 2017 dhamel1. All rights reserved.
//

#import "GameScene.h"
#define SK_DEGREES_TO_RADIANS(__ANGLE__) ((__ANGLE__) * 0.01745329252f)
#define shipBulletName @"shipBullet"
#define asteroidName @"Asteroid"
#define bulletSize CGSizeMake(4, 8)
typedef NS_OPTIONS(uint32_t, CollisionCategory) {
    CollisionCategoryShip = 0x1 << 1,
    CollisionCategoryNumber = 0x1 << 2,
};

@interface GameScene () <SKPhysicsContactDelegate> {
    SKSpriteNode *ship;
    SKSpriteNode *numberNode;
    
    SKLabelNode* scoreLabel;
    SKLabelNode* shipScoreLabel;
    SKLabelNode* gameOverLabel;
    SKLabelNode* restartLabel;
    SKLabelNode* finalScore;
    
    
    //NSMutableArray* tapQueue;
    bool gameOver;
    bool addingMode;
    bool divisableMode;
    bool subtractingMode;
    bool contacted;
    int score;
    int shipScore;
    int delay;
    int durationAsteroid;
}

@end

@implementation GameScene {
    
}

static const uint32_t shipCategory = 1 << 0;
static const uint32_t bulletCategoy = 1 << 1;
static const uint32_t asteroidCategory = 1 << 2;

-(void)setUp{
    _tapQueue = [NSMutableArray array];
    _locationQueue = [NSMutableArray array];
    self.userInteractionEnabled = YES;
    gameOver = NO;
    divisableMode = YES;
    contacted = NO;
    score = 0;
    shipScore = 0;
    
    SKTexture *shipTexture = [SKTexture textureWithImageNamed:@"spaceship.png"];
    shipTexture.filteringMode = SKTextureFilteringNearest;
    ship = [SKSpriteNode spriteNodeWithTexture:shipTexture];
    ship.position = CGPointMake(CGRectGetMidX(self.view.frame),CGRectGetMidY(self.view.frame));
    [ship setScale: .15];
    ship.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:shipTexture.size.width/2];
    //ship.physicsBody.collisionBitMask = asteroidCategory;
    ship.physicsBody.categoryBitMask = shipCategory;
    //ship.physicsBody.collisionBitMask = asteroidCategory;
    ship.physicsBody.dynamic = NO;
    [self addChild:ship];
    self.physicsWorld.contactDelegate = self;
    
    shipScoreLabel = [SKLabelNode labelNodeWithFontNamed:@"ChalkDuster"];
    CGPoint ssLabelPos = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    shipScoreLabel.position = ssLabelPos;
    shipScoreLabel.text = [NSString stringWithFormat:@"%d", shipScore];
    [self addChild:shipScoreLabel];
    
    
    gameOverLabel = [SKLabelNode labelNodeWithFontNamed:@"ChalkDuster"];
    CGPoint gameOverPos = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)+150);
    gameOverLabel.position = gameOverPos;
    gameOverLabel.text = [NSString stringWithFormat:@"Game Over"];
    gameOverLabel.hidden = YES;
    [self addChild:gameOverLabel];
    
    restartLabel = [SKLabelNode labelNodeWithFontNamed:@"ChalkDuster"];
    CGPoint restartPos = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)+100);
    restartLabel.position = restartPos;
    restartLabel.text = [NSString stringWithFormat:@"Click to Restart"];
    restartLabel.hidden = YES;
    [self addChild:restartLabel];
}

-(SKNode*)makeBullet {
    SKNode* bullet;
    bullet = [SKSpriteNode spriteNodeWithColor:[SKColor whiteColor] size:bulletSize];
    bullet.name = shipBulletName;
    bullet.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:bullet.frame.size];
    bullet.physicsBody.categoryBitMask = bulletCategoy;
    bullet.physicsBody.collisionBitMask = asteroidCategory;
    bullet.physicsBody.contactTestBitMask = asteroidCategory;
    bullet.physicsBody.dynamic= NO;
    return bullet;
}

- (CGPoint)randomPointOutsideRect:(CGRect)rect {
    NSUInteger random = arc4random_uniform(4);
    UIRectEdge edge = 1 << random; // UIRectEdge enum values are defined with bit shifting
    
    CGPoint randomPoint = CGPointZero;
    if (edge == UIRectEdgeTop || edge == UIRectEdgeBottom) {
        randomPoint.x = arc4random_uniform(CGRectGetWidth(rect)) + CGRectGetMinX(rect);
        if (edge == UIRectEdgeTop) {
            randomPoint.y = CGRectGetMinY(rect) - 100;
        }
        else {
            randomPoint.y = CGRectGetMaxY(rect) + 100;
        }
    }
    else if (edge == UIRectEdgeLeft || edge == UIRectEdgeRight) {
        randomPoint.y = arc4random_uniform(CGRectGetHeight(rect)) + CGRectGetMinY(rect);
        if (edge == UIRectEdgeLeft) {
            randomPoint.x = CGRectGetMinX(rect) - 100;
        }
        else {
            randomPoint.x = CGRectGetMaxX(rect) + 100;
        }
    }
    
    return randomPoint;
}

-(void)generateEnemyNumbers{
    //while(!gameOver){
        //SKNode* numberHolder;
        //numberHolder = [SKSpriteNode spriteNodeWithColor:[SKColor whiteColor] size:bulletSize];
        //numberHolder.name = @"Asteroid";
    CGPoint  point = [self randomPointOutsideRect:self.frame];
    SKNode * numberHolder;
    numberHolder.name = asteroidName;
    SKTexture *asteroidTexture = [SKTexture textureWithImageNamed:@"asteroid2.png"];
    asteroidTexture.filteringMode = SKTextureFilteringNearest;
    numberHolder = [SKSpriteNode spriteNodeWithTexture:asteroidTexture];
    [numberHolder setScale:.2];
    numberHolder.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:numberHolder.frame.size.width/2];
    numberHolder.physicsBody.categoryBitMask = asteroidCategory;
    numberHolder.physicsBody.collisionBitMask = bulletCategoy;
    numberHolder.physicsBody.contactTestBitMask = bulletCategoy;
    //numberHolder.physicsBody.dynamic = NO;
    float x = arc4random_uniform((CGRectGetMaxX(self.view.frame))) + CGRectGetMaxX(self.view.frame);
    float y = arc4random_uniform((CGRectGetMaxY(self.view.frame))) + CGRectGetMaxY(self.view.frame);
    numberHolder.position = point;
    //[numberHolder setName:@"numberLabel"];
    
    int value = arc4random_uniform(20);
    SKLabelNode * numberLabel;
    numberLabel = [SKLabelNode labelNodeWithFontNamed:@"ChalkDuster"];
    numberLabel.fontSize = 130;
    if(numberHolder == nil){
        NSLog(@"number holder is null");
    }
    //label = [[SKLabelNode alloc] init];
    [numberHolder addChild:numberLabel];
    [numberLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeCenter];
    [numberLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    numberLabel.text = [NSString stringWithFormat:@"%d", value];
    [numberLabel setName:@"numberLabel"];
    //numberLabel.center = (CGPointMake(x,y));
    CGPoint  destination = CGPointMake(0,0);
    if((point.x < CGRectGetMidX(self.frame)) && (point.y < CGRectGetMidY(self.frame))){
        destination = CGPointMake(CGRectGetMaxX(self.frame), CGRectGetMaxY(self.frame));
    }
    
    if((point.x > CGRectGetMidX(self.frame)) && (point.y < CGRectGetMidY(self.frame))){
        destination = CGPointMake(CGRectGetMinX(self.frame), CGRectGetMaxY(self.frame));
    }
    
    if((point.x < CGRectGetMidX(self.frame)) && (point.y > CGRectGetMidY(self.frame))){
        destination = CGPointMake(CGRectGetMaxX(self.frame), CGRectGetMinY(self.frame));
    }
    
    if((point.x > CGRectGetMidX(self.frame)) && (point.y > CGRectGetMidY(self.frame))){
        destination = CGPointMake(CGRectGetMinX(self.frame), CGRectGetMinY(self.frame));
    }
    
    SKAction* numberAction = [SKAction sequence:@[[SKAction moveTo:destination duration:2.5],
                                                    [SKAction waitForDuration:3.0/60.0],
                                                      [SKAction removeFromParent]]];
    [numberHolder runAction:numberAction];
    if(numberLabel == nil){
        NSLog(@"number label is null");
    }
    [self addChild:numberHolder];
    float randomNum = arc4random_uniform(4) * 0.4 + 0.25;
    [self performSelector:@selector(generateEnemyNumbers) withObject:nil afterDelay:2];
    //[numberHolder addChild:numberLabel];
    //}
}


-(void)fireBullet:(SKNode*)bullet toDestination:(CGPoint)destination withDuration:(NSTimeInterval)duration {
    NSLog(@"fired bullet");
    SKAction* bulletAction = [SKAction sequence:@[[SKAction moveTo:destination duration:duration],
                                                  [SKAction waitForDuration:3.0/60.0],
                                                  [SKAction removeFromParent]]];
    //SKAction* soundAction  = [SKAction playSoundFileNamed:soundFileName waitForCompletion:YES];
    //[bullet runAction:[SKAction group:@[bulletAction, soundAction]]];
    [bullet runAction: bulletAction];
    [self addChild:bullet];
}

-(void)fireShipBullets:(CGPoint*)destination {
    SKNode* existingBullet = [self childNodeWithName: shipBulletName];
    //1
    if (!existingBullet) {
        @try{
            //CGPoint *destination =(__bridge CGPoint *)(_locationQueue[0]);
            [_locationQueue removeObjectAtIndex:0];
            //SKNode* ship = [self childNodeWithName:shipName];
            SKNode* bullet = [self makeBullet];
            //2
            bullet.position = CGPointMake(ship.position.x, ship.position.y);
            //3
            //CGPoint bulletDestination = CGPointMake(ship.position.x, self.frame.size.height + bullet.frame.size.height / 2);
            //4
            [self fireBullet:bullet toDestination:*destination withDuration:.5];
        }@catch(NSException *e){
            NSLog(@"Destination: %@",NSStringFromCGPoint(*destination));
        }
            
        @finally{}
    }
}

- (void)didMoveToView:(SKView *)view {
    NSLog(@"Setting up");
    [self setUp];
    [self generateEnemyNumbers];
}


- (void)touchDownAtPoint:(CGPoint)pos {
    NSLog(@"Touch down occured");
    //for (UITouch *touch in touches) {
    //CGPoint positionInScene = [touch locationInNode:self];
    
    float deltaX = pos.x - ship.position.x;
    float deltaY = pos.y - ship.position.y;
    
    float angle = atan2f(deltaY, deltaX);
    NSLog(@"%f",angle);
    //ship.zRotation = angle - SK_DEGREES_TO_RADIANS(90.0f);
    //}
}

- (void)touchMovedToPoint:(CGPoint)pos {
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
   
    UITouch *touch = [touches anyObject];
    //NSLog(@"Touch registered at %@: ",touch);
    CGPoint positionInScene = [touch locationInNode:self];
    
    float deltaX = positionInScene.x - ship.position.x;
    float deltaY = positionInScene.y - ship.position.y;
    
    float angle = atan2f(deltaY, deltaX);
    NSLog(@"%f",angle);
    ship.zRotation = angle;// - SK_DEGREES_TO_RADIANS(90.0f);
    //positionInScene.x = positionInScene.x *2;
    //positionInScene.y = positionInScene.y * 2;
    contacted = NO;
    if(gameOver){
        gameOver = NO;
        [self cleanSlate];
    }
    
    if(!gameOver){
        [self fireShipBullets:&positionInScene];
    }
}
- (void)touchUpAtPoint:(CGPoint)pos {

}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch* touch = [touches anyObject];
    CGPoint positionInScene = [touch locationInNode:self];
    if (touch.tapCount == 1){
        [self.tapQueue addObject:@1];
        [self.locationQueue addObject:[NSValue valueWithCGPoint:positionInScene]];
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    /*for (UITouch *touch in touches) {
        CGPoint positionInScene = [touch locationInNode:self];
        
        float deltaX = positionInScene.x - ship.position.x;
        float deltaY = positionInScene.y - ship.position.y;
        
        float angle = atan2f(deltaY, deltaX);
        NSLog(@"%f",angle);
        ship.zRotation = angle;// - SK_DEGREES_TO_RADIANS(90.0f);
    }*/
}

-(void)processUserTapsForUpdate:(NSTimeInterval)currentTime {
    //NSLog(@"processing taps");
    for (NSNumber* tapCount in [self.tapQueue copy]) {
        NSLog(@"looping through queue of taps");
        if ([tapCount unsignedIntegerValue] == 1) {
            NSLog(@"Fired ship bullet");
            //[self fireShipBullets];
        }
        [self.tapQueue removeObject:tapCount];
    }
}
-(void)handleDivisableScoring:(NSInteger)valueIn{
    if(!contacted){
        if(shipScore == 0){
            shipScore = (int)valueIn;
            shipScoreLabel.text = [NSString stringWithFormat:@"%d", shipScore];
            contacted = YES;
        }else{
            if(valueIn == 0){
                gameOver = YES;
            }else if(shipScore % valueIn == 0){
                shipScore = shipScore + (int)valueIn;
                score++;
                shipScoreLabel.text = [NSString stringWithFormat:@"%d", shipScore];
                contacted = YES;
            }else{
                gameOver=YES;
            }
        }
    }
}

-(void)didBeginContact:(SKPhysicsContact *)contact{
    NSLog(@"Contact occured");
    //NSLog(@"%@",contact.bodyB.node.name);
    
    SKLabelNode *tempNode = (SKLabelNode*)[contact.bodyB.node childNodeWithName:@"numberLabel"];
    contact.bodyB.node.hidden = YES;
    contact.bodyA.node.hidden = YES;
    NSLog(@"tempNode Name: %@",tempNode.name);
    NSString *text = tempNode.text;
    NSInteger value = [text integerValue];
    [self handleDivisableScoring:value];
    SKPhysicsBody* firstBody;
    SKPhysicsBody* secondBody;
    if (contact.bodyA.categoryBitMask > contact.bodyB.categoryBitMask) {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    } else {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    if([firstBody.node.name  isEqual: shipBulletName]){
        //bottomOnTable = 1;
        NSLog(@"Bullet");
    }
    
    if([firstBody.node.name  isEqual: @"Asteroid"]){
        //topOnTable = 1;
        NSLog(@"Asteroid");
    }
    if(divisableMode){
        
    }
    if(addingMode){
        
    }
    if(subtractingMode){
        
    }
}

-(void)cleanSlate{
    gameOverLabel.hidden = YES;
    restartLabel.hidden = YES;
    shipScore = 0;
    score = 0;
    shipScoreLabel.text = [NSString stringWithFormat:@"%d", shipScore];
}

-(void)gameOver{
    NSLog(@"Game over");
    gameOverLabel.hidden = NO;
    restartLabel.hidden = NO;

}

-(void)update:(CFTimeInterval)currentTime {
    // Called before each frame is rendered
    [self processUserTapsForUpdate:currentTime];
    if(gameOver) {
        [self gameOver];
    }
}

@end
